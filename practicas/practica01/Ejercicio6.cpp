/**
 * UNIVERSIDAD IBEROAMERICANA
 * TSU SOFTWARE
 * GRUPO A
 * ASIGNATURA:DESARROLLO DE APLICACIONES II
 * LENGUAJE:C++
 * Una subrutina que implemente la ordenación de la burbuja (“Quicksort”)
 * 𝑋 = {9,6,7,2,5,3}
 * */




#include <iostream>
#define max 1000

using namespace  std;

void quicksort(int [],int);
void imprimir(int[],int);

int main()
{
    cout << "Ingresa n numeros " << endl;
    int n;
    cin >>n;
    int a[n];
    for(int i= 0;i < n;i++)
{
    cout << "Dame numero "<< i+1 << " :\n -->";
    cin >>a[i];
}

quicksort(a,n);
imprimir(a,n);

return 0;
}

void imprimir(int a[],int tamano)
{
    cout << "Arreglo de numeros ordenados.\n \n";
    for(int i=0;i < tamano;i++){
        cout << "\t[" << a[i] << "] ";
    }

}
void quicksort(int a[],int n)
{
    int tope ,inicio ,fin, pos;
        int may[max],menor[max];
        tope =0;
        menor[tope]=0;
        may[tope]=n-1;
        while(tope >=0)
        {
            inicio = menor[tope];
            fin = may[tope];
            tope--;
            /////////////
            int izq,der,aux;
            bool bandera;
            izq=inicio;
            der=fin;
            pos=inicio;

            bandera = true;
            while (bandera==true)
            {
                while((a[pos]<a[der]) && (pos != der))
                    der--;
                if(pos==der)
                    bandera = false;
                else
                {
                    aux=a[pos];
                    a[pos]= a[der];
                    a[der] = aux;
                    pos = der;
                }

                while((a[pos]>a[izq]) && (pos != izq)){
                    izq++;

                    if(pos == izq)
                        bandera = false;
                    else
                    {
                        aux = a[pos];
                        a[pos] = a[izq];
                        a[izq] = aux;
                        pos=izq;

                    }
                }
                if(inicio <= (pos-1))
                {
                    tope++;
                    menor[tope] = inicio;
                    may[tope] = pos-1;

                }
                if(fin >=(pos+1))
                {
                    tope++;
                    menor[tope]=pos+1;
                    may[tope]=fin;
                }

            }

        }
}