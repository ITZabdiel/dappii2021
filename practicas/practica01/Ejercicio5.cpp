/**
 * UNIVERSIDAD IBEROAMERICANA
 * TSU SOFTWARE
 * GRUPO A
 * ASIGNATURA:DESARROLLO DE APLICACIONES II
 * LENGUAJE:C++
 * Una subrutina que implemente la ordenación de la burbuja (“Bubblesort”)
 * 𝑋 = {9,6,7,2,5,3}
 * */


#include <iostream>

using namespace std;

//prototipos

void bubblesort(int [], int);
void imprimir(int[],int);

int main(){

    //llamado a los metodos bubblesort
    int a[]={9,6,7,2,5,3};
    bubblesort(a,6);
    imprimir(a,6);

    return 0;
}

void bubblesort(int a[], int n)
{
    //variable auxiliara para el intercambio de elementos
    int aux,i,j;
    //ciclo for
    for(int i=1;i<=n;i++)
    {
        //
        for(j = n;j>=i;j--)
        {
            //compraciones de elementos
            if(a[j-1]>a[j])
            {
                aux = a[j-1];
                a[j-1]=a[j];
                a[j]= aux;
            }
        }
    }
}

void imprimir(int a[],int n)
{
    cout << "Numeros ordenados: " << endl;
    for(int i= 0;i<n;i++)
    {
        cout << "[" << a[i]<< "]";
    }

}