/**
 * UNIVERSIDAD IBEROAMERICANA
 * TSU SOFTWARE
 * GRUPO A
 * ASIGNATURA:DESARROLLO DE APLICACIONES II
 * LENGUAJE:C++
 * Dada la siguiente expresión: ∑5 𝑘2 𝑘=1
 * Realizar un programa o una subrutina que realice el cómputo de la sumatoria.
 * **/

void sumatoria();

#include <iostream>
#include <math.h>


using namespace std;
//prototipo de la funcion
void suma(int a);
int main(){
    int a = 5;
    cout << "Entero ingresado para la sumatoria:  " << a << endl;

    suma(a);

    return 0;
}
//funcion sumatoria
void suma(int a){

    for(int i = 1;i <= 5; i++){
        int suma;
         //a+=pow(i,2);
         a+=(i*i);
        cout <<"posicion: " << i << ","<< " Elevado al cuadrado "<< i << "*" << i << " = "
        << "["<< (i*i) <<"]"  << " = "
        << a << endl;
    }
}