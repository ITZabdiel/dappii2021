/**
 * UNIVERSIDAD IBEROAMERICANA
 * TSU SOFTWARE
 * GRUPO A
 * ASIGNATURA:DESARROLLO DE APLICACIONES II
 * LENGUAJE:C++
 * Realizar una función que calcule el área de un círculo.
 * paso por referencia
 * **/

#include <iostream>
#include <math.h>
using namespace std;

//prototipo
void areaCirculo(double radio,double &area);

int main() {
    double area = 0;
    double radio =0;
    cout << "Valor de area original "<< area << " referencia: "<< &area <<endl;
    cout << "Ingresa el radio: "<< endl;
    cin >>radio;
    //se llama la funcion areacirculo
    areaCirculo(radio,area);
    cout << "La referencia es : " << &area << "el nuevo valor es: " << area << endl;

    return 0;
}

//metodo area circulo paso por referencia
void areaCirculo(double radio,double &area) {

    double pi = 3.1416;
    radio = pow(radio, 2);
    area = (pi * radio);
    cout << "El area es: " << area << endl;
}