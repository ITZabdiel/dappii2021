/**
 * UNIVERSIDAD IBEROAMERICANA
 * TSU SOFTWARE
 * GRUPO A
 * ASIGNATURA:DESARROLLO DE APLICACIONES II
 * LENGUAJE:Java
 * Realizar una función que calcule el área de un círculo.
 * Tablas de multiplicar
 * **/



package EjercicioDos;

import java.util.Scanner;

/**
 * Utilizando algún lenguaje de programación, realice una subrutina para imprimir las tablas de multiplicar del 1 al 10.
 * */
public class Ejerciciodos {

    public static void main (String[] Args){


        System.out.println("Tabla de multiplicar");
        //LAMADA AL METODO MULTIPLICACION
        //SE SOLICITA INGRESAR UN NUMERO
        multiplicacion();

    }
/**
 * DECLARACION DEL METODO MULTIPLICACION
 *
 * */
    public static void multiplicacion(){
        int resultado=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingresa un numero");
        int opcion = scanner.nextInt();
        if(opcion == 0 || opcion > 10){
            System.out.println("Ingresa un numero valido entre uno al diez");
    }else{
            for(int i= 1;i <= 10;i++){
                resultado = i * opcion;
                System.out.println(i +" X " + opcion + " = " + resultado);
            }
        }
    }

}