/**
 * UNIVERSIDAD IBEROAMERICANA
 * TSU SOFTWARE
 * GRUPO A
 * ASIGNATURA:DESARROLLO DE APLICACIONES II
 * LENGUAJE:Java
 * Realizar una función que calcule el área de un círculo.
 * paso por valor
 * **/

package EjercicioTres;
import java.util.Scanner;


public class EjercicioTres {

    public static void main(String[] args) {

        /***
         * Solicitud de datos al usuario
         */

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingresa el radio");
        double numero = scanner.nextDouble();
        //llamada al metodo areaCirculo
        areaCirculo(numero);
        System.out.println("Paso por valor " + numero);

    }
    //metodo area circulo
    public static void areaCirculo(double numero) {

        double pi = 3.1416;
        double radio = Math.pow(numero, 2);
        double area = (pi * radio);
        System.out.println("El area es: " + area);
    }
}
