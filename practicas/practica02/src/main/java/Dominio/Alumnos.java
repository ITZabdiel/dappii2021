/***
 * <h1>Alumnos</h1>
 * Clase mapeada con los atributos de la clase
 * <p>
 * @author Ittai Zabdiel Vazquez Rosales
 * @version 1.0
 * @since 2021
 */

package Dominio;


public class Alumnos {

    private Integer matricula;
    private String nombre;
    private String apellido;
    private Integer grado;

    public Alumnos() {
    }

    public Alumnos(Integer matricula, String nombre, String apellido, Integer grado) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.grado = grado;
    }

    public Alumnos(Integer matricula) {
        this.matricula = matricula;
    }

    public Integer getMatricula() {
        return matricula;
    }

    public void setMatricula(Integer matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) {
        this.grado = grado;
    }

    @Override
    public String toString() {
        return "Alumnos{" +
                "matricula=" + matricula +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", grado=" + grado +
                '}';
    }
}
