/***
 * <h1>AlumnosDao</h1>
 * Se crea la clase Alumnos Dao ,ya que desde esta clase se realizara el acceso y cambios en la base de datos en conjunto con la clase persona
 *
 * <p>
 * Con el fin de instanciarse y utilizarse em conunto con la base de datos
 * @author Ittai Zabdiel Vazquez Rosales
 * @version 1.0
 * @since 2021
 */

package AlumnosDao;

import DB.Conexion;
import Dominio.Alumnos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static DB.Conexion.*;


public class AlumnoDao {


        /**
         * Se implementa static los querys del CRUD
         * */
        private static final String select = "SELECT matricula,nombre,apellido,grado FROM alumnos";
        private static final String insert = "INSERT INTO alumnos(matricula,nombre,apellido,grado) VALUES(? ,? ,? ,?)";
        private static final String update = "UPDATE alumnos SET nombre =?,apellido = ?,grado = ? = ? WHERE matricula = ?";
        private static final String delete = "DELETE FROM alumnos WHERE matricula = ? ";

        /**
         * Read
         * */
        public List<Alumnos> seleccionar(){
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            Alumnos alumno= null;
            List<Alumnos> alumns = new ArrayList<Alumnos>();

            try {
                conn = conexion();
                preparedStatement = conn.prepareStatement(select);
                resultSet = preparedStatement.executeQuery();
                while(resultSet.next()){
                    Integer matricula = resultSet.getInt("Matricula");
                    String nombre = resultSet.getString("nombre");
                    String apellido = resultSet.getString("apellido");
                    Integer grado = resultSet.getInt("grado");


                    /**
                     * Se crea un objeto alumno
                     * */
                    alumno = new Alumnos(matricula,nombre,apellido,grado);
                    /**
                     * Se agrega a la lista alumns el objeto alumno
                     * */
                    alumns.add(alumno);
                }
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
            finally {
                try {
                    close(resultSet);
                    close(preparedStatement);
                    close(conn);
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }

            return alumns;
        }

        /**
         * Create
         * */
        public int insertar(Alumnos alumno){
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            int registro = 0;
            try {
                /**
                 *
                 * */
                conn = Conexion.conexion();
                preparedStatement = conn.prepareStatement(insert);
                preparedStatement.setInt(1,alumno.getMatricula());
                preparedStatement.setString(2,alumno.getNombre());
                preparedStatement.setString(3,alumno.getApellido());
                preparedStatement.setInt(4,alumno.getGrado());

                /**
                 * regresa el numero de registros afectados
                 * */
                registro = preparedStatement.executeUpdate();
            } catch (SQLException exception) {
                exception.printStackTrace(System.out);
            }
            finally {
                try {
                    close(preparedStatement);
                    close(conn);
                } catch (SQLException exception) {
                    exception.printStackTrace(System.out);
                }

            }
            return registro;
        }
        /**
         * Update
         * */
        public int actualizar(Alumnos alumnos){
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            int registro = 0;
            try {
                /**
                 *
                 * */
                conn = Conexion.conexion();
                preparedStatement = conn.prepareStatement(update);
                preparedStatement.setString(1,alumnos.getNombre());
                preparedStatement.setString(2,alumnos.getApellido());
                preparedStatement.setInt(3,alumnos.getGrado());
                preparedStatement.setInt(4,alumnos.getMatricula());

                /**
                 * regresa el numero de registros afectados
                 * */
                registro = preparedStatement.executeUpdate();
            } catch (SQLException exception) {
                exception.printStackTrace(System.out);
            }
            finally {
                try {
                    close(preparedStatement);
                    close(conn);
                } catch (SQLException exception) {
                    exception.printStackTrace(System.out);
                }

            }
            return registro;
        }

        /**
         * Delete
         * */
        public int eliminar(Alumnos alumnos){
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            int registro = 0;
            try {
                /**
                 *
                 * */
                conn = Conexion.conexion();
                preparedStatement = conn.prepareStatement(delete);
                preparedStatement.setInt(1,alumnos.getMatricula());
                /**
                 * regresa el numero de registros afectados
                 * */
                registro = preparedStatement.executeUpdate();
            } catch (SQLException exception) {
                exception.printStackTrace(System.out);
            }
            finally {
                try {
                    close(preparedStatement);
                    close(conn);
                } catch (SQLException exception) {
                    exception.printStackTrace(System.out);
                }

            }
            return registro;
        }

    }


