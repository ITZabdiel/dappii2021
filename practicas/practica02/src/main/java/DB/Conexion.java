

package DB;

import java.sql.*;

public class Conexion {

    /**
     * Se se implementan los parametros de conexion staticos
     * */
    private static final String jdbc = "jdbc:mysql://localhost:3306/calificaciones?useSSL=false&useTimezone=UTC&allowPublicKeyRetrieval=True";
    private static final String user = "root";
    private static final String pass = "Test2020";

    //metodos staticos
    public static Connection conexion() throws SQLException {
        return DriverManager.getConnection(jdbc,user,pass);
    }
    public static void close(ResultSet rs) throws SQLException {
        rs.close();
    }

    public static void close(Statement st) throws SQLException {
        st.close();
    }
    public static void close(PreparedStatement ps) throws SQLException {
        ps.close();
    }
    public static void close(Connection conn) throws SQLException {
        conn.close();
    }
}
